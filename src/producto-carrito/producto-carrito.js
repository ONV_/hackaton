import { LitElement, html } from "lit-element";

class ProductoCarrito extends LitElement{
    
    static get properties() {
        return {
            id: {type: String},
            name: {type: String},
            desc: {type: String},
            price: {type: Number},
            photo: {type: String},
            qtt: {type: Number}
        };
    }

    updated(changedProperties) {
        if (changedProperties.has("qtt")) {
            this.shadowRoot.getElementById("inputnum").value = this.qtt;
            console.log("updated qtt DENTRO DEL CARRITO valor: " + this.qtt)
        }
    }

    constructor() {
        super();
        console.log("producto-carrito")
    } 

    render() {
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <!--  
                <li>
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <h3>${this.name}</h3>
                            </div>                 
                        </div>
                        <div class="row">
                            <div class="col-sm">
                                <input id="inputnum" @input="${this.updateQtt}" type="number" value="${this.qtt}"><h5>unidades</h5>
                            </div>       
                            <div class="col-sm">
                                <button @click="${this.deleteProducto}" class="btn btn-danger">Eliminar</button>
                            </div>
                        </div>
                    </div>
                </li>
            -->
            <table class="table table-bordered border-primary">  
                    <tr class="table-info">  
                        <th> <h4>${this.name}</h4> </th> 
                        <th> <input id="inputnum" @input="${this.updateQtt}" 
                             type="Number" 
                             style="width:50px;height:20px"
                             value="${this.qtt}"><h6>unidades</h6> </th> 
                        <th> <button @click="${this.deleteProducto}" class="btn btn-danger">Eliminar</button> </th>
                    </tr>
            </table>   
        `;
    }

    deleteProducto() {
        console.log("deleteProducto a producto-carrito")
        this.dispatchEvent(
            new CustomEvent(
                "delete-producto",
                {
                    detail : {
                        id: this.id
                    }
                }
            )
        )
    }

    updateQtt(e) {
        this.qtt = e.target.value;

        console.log("updateQtt "+this.qtt)

        this.dispatchEvent(
            new CustomEvent(
                "update-qtt",
                {
                    detail : {
                        product : {
                            id: this.id,
                            name : this.name,
                            desc : this.desc,
                            price : this.price,
                            photo : this.photo,
                            qtt: e.target.value
                        }  
                    }
                }
            )
        )
    }
}

customElements.define('producto-carrito', ProductoCarrito);