import { LitElement, html } from "lit-element";

class ProductoFicha extends LitElement{
    
    static get properties() {
        return {
            id: {type: String},
            name: {type: String},
            desc: {type: String},
            price: {type: Number},
            photo: {type: String},
            qtt: {type: Number},
            stock: {type: Number}
        };
    }

    constructor() {
        super();

        this.qtt = 1;
    } 

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            
            <div class="card" style="width: 18rem;">
                <img src="${this.photo}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">${this.name}</h5>
                    <p class="card-text">${this.desc}</p>
                    <p class="card-text">${this.price} €</p>
                    <button @click="${this.addToCart}" class="btn btn-primary"><strong>AÑADIR</strong></button>
                    <p> </p>
                    <input @input="${this.addQtt}" type="number" min="1" max="${this.stock}" placeholder="1"
                    class="input-group card-footer" style="width:80px;height:50px"/>
                </div>
            </div>
        `;
    }

    addQtt(e) {
        this.qtt = e.target.value;
    }

    addToCart() {
        this.dispatchEvent(
            new CustomEvent(
                "add-to-cart",
                {
                    detail : {
                        id: this.id,
                        name: this.name,
                        desc: this.desc,
                        price: this.price,
                        photo: this.photo,
                        qtt: this.qtt
                    }
                }
            )
        )
    }
}

customElements.define('producto-ficha', ProductoFicha);