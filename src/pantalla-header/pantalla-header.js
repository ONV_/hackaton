import { LitElement, html } from "lit-element";

class PantallaHeader extends LitElement{
    
    static get properties() {
        return {
            data: {type: String} 
        };
    }

    constructor() {
        super();
        console.log("pantalla-header")
    } 

    render() {
        return html`
            <div align="center">
                <img src="../assets/logohackaton.jpg" />
                <h1>Nuestra tienda de chuches</h1>
                <p></p>
            </div>
        `;
    }
}

customElements.define('pantalla-header', PantallaHeader);