import { LitElement, html } from "lit-element";
import "../pantalla-main/pantalla-main.js";
import "../pantalla-header/pantalla-header.js";
import "../pantalla-footer/pantalla-footer.js";
import "../pantalla-sidebar/pantalla-sidebar.js";
import "../pantalla-modal/pantalla-modal.js";

class PantallaApp extends LitElement{
    
    static get properties() {
        return {
            product : {type : Object}
        };
    }

    updated(changedProperties) {
        if (changedProperties.has("product")) {
            console.log("se ha añadido producto pantalla-app");
            console.log(this.product);

            this.shadowRoot.querySelector("pantalla-sidebar").lastProduct = this.product;
        }
    }

    constructor() {
        super();

        console.log("pantalla-app")
    } 

    render() {
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <div class="container">
                <div class="row">
                    <pantalla-header></pantalla-header>
                </div>
                <div class="row">
                    <pantalla-main @add-to-cart-main="${this.addToSidebar}" class="col-10"></pantalla-main>
                    <pantalla-sidebar @refresh-data="${this.refreshData}" class="col-2"></pantalla-sidebar>
                </div>
                <div class="row">
                    <pantalla-footer></pantalla-footer>
                </div>    
            </div>
        `;
    }

    addToSidebar(e) {
        console.log("addToSidebar");
        console.log("quantity " + e.detail.product.qtt);

        this.product = {
            id : e.detail.product.id,
            name : e.detail.product.name,
            desc : e.detail.product.desc,
            price : e.detail.product.id,
            photo : e.detail.product.photo,
            qtt: e.detail.product.qtt
        }
    }

    refreshData() {
        console.log("refreshData");
        //this.shadowRoot.querySelector("pantalla-main").updateData = true;
        window.location.reload();
    }
}

customElements.define('pantalla-app', PantallaApp);