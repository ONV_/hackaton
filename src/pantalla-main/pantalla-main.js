import { LitElement, html } from "lit-element";
import "../pantalla-main-dm/pantalla-main-dm.js";
import "../producto-ficha/producto-ficha.js";

class PantallaMain extends LitElement{
    
    static get properties() {
        return {
            listProducts: {type: Array},
            updateData: {type: Boolean} 
        };
    }

    updated(changedProperties) {
        if (changedProperties.has("updateData")) {
            console.log("se va a refrescar pagina pantalla-main");
            this.shadowRoot.querySelector("pantalla-main-dm").updateData = this.updateData;
        }
    }

    constructor() {
        super();
        console.log("pantalla-main")
        
        this.listProducts = [];
        
    } 

    render() {
        return html`
            <div class="container"> 
                <pantalla-main-dm @listProducts-updated="${this.listProductsUpdated}"></pantalla-main-dm>
                <div class="row" id="productList" align="center">
                    <div class="row row-cols-1 row-cols-sm-4">
                        ${this.listProducts.map(
                            product => html`<producto-ficha 
                                id="${product.id}"
                                name="${product.name}"
                                desc="${product.desc}"
                                .photo="${product.foto}" 
                                price="${product.price}"
                                stock="${product.stock}"
                                @add-to-cart="${this.addToCart}"
                                ></producto-ficha><p> </p>` 
                        )}
                    </div>
                </div>
            </div>  
        `;
    }

    listProductsUpdated(e) {
        console.log("listProductsUpdated en pantalla-main");
        this.listProducts = e.detail.listProducts;
        console.log(JSON.stringify(this.listProducts) + "en pantalla main");
    }

    addToCart(e) {
        console.log("addToCart "+e.detail);
        this.dispatchEvent(
            new CustomEvent(
                "add-to-cart-main",
                {
                    detail : {
                        product : {
                            id : e.detail.id,
                            name : e.detail.name,
                            desc : e.detail.desc,
                            price : e.detail.price,
                            photo : e.detail.photo,
                            qtt : e.detail.qtt
                        }
                    }
                }
            ) 
        )
    }
}

customElements.define('pantalla-main', PantallaMain);