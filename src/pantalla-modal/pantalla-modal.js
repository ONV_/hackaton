import { LitElement, html } from "lit-element";

class PantallaModal extends LitElement{
    
    static get properties() {
        return {
            amount: {type: Number} 
        };
    }

    constructor() {
        super();
        console.log("pantalla-modal")
    } 

    render() {
        return html`
        <!-- Modal -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

        <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="Compra realizada! aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="modalTitle">Compra realizada</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                Tu compra ha sido de: ${this.amount}
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Aceptar</button>
              </div>
            </div>
          </div>
        </div>
        `;
    }
}

customElements.define('pantalla-modal', PantallaModal);