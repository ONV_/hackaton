import { LitElement, html } from "lit-element";

class PantallaMainDM extends LitElement{
    
    static get properties() {
        return {
            listProducts: {type: Array},
            updateData: {type: Boolean}
        };
    }

    constructor() {
        super();
        console.log("pantalla-main-dm")

        this.listProducts = this.getListProducts();
        /*this.listProducts =
            [
                {"id":"1",
                "name":"Regaliz",
                "desc":"Regaliz negro enrollado",
                "price":0.1,
                "foto":"https://www.lacasadelasgolosinas.com/3801-large_default/discos-negros-regaliz-haribo-2kg.jpg",
                "stock":100},
                {"id":"2",
                "name":"Calaveras",
                "desc":"Calaveras de pica pica",
                "price":0.5,
                "foto":"https://www.lacasadelasgolosinas.com/7669-large_default/rellenolas-vidal-calaveras-65u.jpg",
                "stock":200}
            ];*/
            console.log(this.listProducts);

    } 

    updated(changedProperties) {
        if (changedProperties.has("listProducts")) {
            console.log("pantalla-main-dm listProducts-updated");
            this.dispatchEvent(
                new CustomEvent(
                    "listProducts-updated",
                    {
                        detail : {
                            listProducts : this.listProducts
                        }
                    }
                )
            )
        }
        if (changedProperties.has("updateData")) {
            console.log("se va a refrescar la pagina pantalla-main-dm");
            this.listProducts = this.getListProducts();
        }
    }

    render() {
        return html`
        `;
    }

    getListProducts() {
        console.log("getListProducts");

        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
            if (xhr.status === 200) {
                console.log("Petición completada correctamente");
                //console.log(JSON.parse(xhr.responseText));

                let APIResponse = JSON.parse(xhr.responseText);
                
                console.log("API Response " + APIResponse);

                this.listProducts = APIResponse;

                //this.listProducts = xhr.responseText;

                console.log("List products pantalla-main-dm " + this.listProducts);
            }
        }

        xhr.open("GET", "http://localhost:8080/productos");
        xhr.send();

        console.log("Fin de getListProducts()");
    }
}

customElements.define('pantalla-main-dm', PantallaMainDM);