import { LitElement, html } from "lit-element";

class PantallaFooter extends LitElement{
    
    static get properties() {
        return {
            
        };
    }

    constructor() {
        super();
        console.log("pantalla-footer")
    } 

    render() {
        return html`
    <div class="container">
        <div class="row" align="center" style="background-color: #92a8d1;">
            <h3>Nuestra tienda de Chuches</h3>
            <p>Curso Practitioner BackEnd + Frontend</p>
            <p>Octubre 2021</p>
        </div>
    </div>
        `;
    }
}

customElements.define('pantalla-footer', PantallaFooter);