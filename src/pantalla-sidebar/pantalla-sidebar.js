import { LitElement, html } from "lit-element";
import "../producto-carrito/producto-carrito.js";

class PantallaSidebar extends LitElement{
    
    static get properties() {
        return {
            listProducts: {type: Array},
            lastProduct: {type: Object},
            idCarrito: {type: Number}
        };
    }

    updated(changedProperties) {
        if(changedProperties.has("lastProduct")) {
            console.log("sidebar se ha añadido algo al carrito");
            console.log("se ha añadido " + JSON.stringify(this.lastProduct));

            //mira si ya existe producto
            let indexOfProduct = this.listProducts.findIndex(
                product => product.id === this.lastProduct.id
            );

            console.log("indexOfProduct "+indexOfProduct);
            console.log(JSON.stringify(this.listProducts));

            if (indexOfProduct != -1) {
                console.log("update producto")
                this.updateProducto(indexOfProduct);
            } else {
                console.log("create producto")
                this.listProducts = [...this.listProducts, this.lastProduct];
            }
            console.log("listProducts tras la operativa " + JSON.stringify(this.listProducts));
        }
        if (changedProperties.has("listProducts")) {
            console.log("listProducts actualizado");
            if (this.listProducts.length > 0) {
                this.shadowRoot.getElementById("btn-guardar").style = ""
            } else {
                this.shadowRoot.getElementById("btn-guardar").style = "display:none;"
            }
        }
    }

    constructor() {
        super();

        this.listProducts = [];
        //this.idCarrito = Math.floor(Math.random() * 200);
        this.idCarrito = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
        console.log("pantalla-sidebar")
    } 

    render() {
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <div class="container">
                <img src="../assets/carritoicon.jpg" width="70" height="70" />
                <p> </p>
                <div class="row" id="productList">
                    <div class="row row-cols-5 row-cols-sm-4">
                        <ul class="list-group" style="list-style: none;">
                            ${this.listProducts.map(
                                product => html`
                                    <producto-carrito 
                                        id="${product.id}"
                                        name="${product.name}"
                                        desc="${product.desc}"
                                        .photo="${product.foto}" 
                                        price="${product.price}"
                                        qtt="${product.qtt}"
                                        @delete-producto="${this.deleteProducto}"
                                        @update-qtt="${this.updateproductoCarrito}"
                                    ></producto-carrito>` 
                            )}
                        </ul>
                    </div>
                    <div class="row">
                        <button @click="${this.storeCarrito}" id="btn-guardar" class="btn btn-success" style="display : none;">Comprar</button>
                    </div>            
                </div>
            </div>
        `;
    }

    updateProducto(indexOfProduct) {
        console.log("updateProducto " + JSON.stringify(this.listProducts[indexOfProduct]));
        console.log("indice " + indexOfProduct);
        let suma = parseInt(this.lastProduct.qtt) + parseInt(this.listProducts[indexOfProduct].qtt);
        this.listProducts[indexOfProduct].qtt = suma;
        this.shadowRoot.getElementById(this.listProducts[indexOfProduct].id).qtt = suma;
    }

    //update producto desde DENTRO del carrito
    updateproductoCarrito(e) {
        console.log("updateproductoCarrito");
        console.log(e);
        let indexOfProduct = this.listProducts.findIndex(
            product => product.id === e.detail.product.id
        );
        this.listProducts[indexOfProduct].qtt = e.detail.product.qtt;
        console.log("update DESDE DENTRO CARRITO, estado de listProducts: " + JSON.stringify(this.listProducts));
        console.log("indice " + indexOfProduct);
    }

    deleteProducto(e) {
        console.log("deleteProducto a sidebar")
        this.listProducts = this.listProducts.filter(
            product => product.id != e.detail.id
        )
    }

    storeCarrito() {
        console.log("storeCarrito");
            
            let purchaseditems = {};
            this.idCarrito += 1;

            for (var i= 0; i < this.listProducts.length; i++) {
                Object.defineProperty(purchaseditems, this.listProducts[i].id, {value : this.listProducts[i].qtt,
                    writable : true,
                    enumerable : true,
                    configurable : true})
            }   

            let carritoFinal =
                {
                    id: this.idCarrito,
                    userid: 2,
                    amount: 0,
                    purchaseItems: purchaseditems
                }    

        console.log("se va a enviar a back el siguiente array: " + JSON.stringify(carritoFinal));

        var xhr = new XMLHttpRequest();
        
        xhr.onload = () => {
            if (xhr.status === 200) {
                console.log("Petición guardado completada correctamente");
                //console.log(JSON.parse(xhr.responseText));

                let APIResponse = JSON.parse(xhr.responseText);
                //APIResponse = JSON.stringify(APIResponse);
                console.log("API Response " + JSON.stringify(APIResponse));
                window.alert("Tu compra se ha realizado correctamente\nprecio total: " + APIResponse.amount + " €");
                //this.listProducts = xhr.responseText;
            } else if (xhr.status === 400) {
                console.log("rechazado API");
                window.alert("No se ha podido realizar la compra");
            }
        }

        xhr.open("POST", "http://localhost:8080/carritos");
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.send(JSON.stringify(carritoFinal));
        
        this.listProducts = [];

        //setTimeout(() => console-log("espera"),1000000);

        let milliseconds = 10000000;
        this.sleep(milliseconds);

        this.refreshData();
    }

    sleep(milliseconds) {
        var start = new Date().getTime();
        for (var i = 0; i < 1e7; i++) {
          if ((new Date().getTime() - start) > milliseconds){
            break;
          }
        }
      }

    refreshData() {
        this.dispatchEvent(
            new CustomEvent(
                "refresh-data",
                {
                    detail: {}
                }
            )
        )
    }
}

customElements.define('pantalla-sidebar', PantallaSidebar);